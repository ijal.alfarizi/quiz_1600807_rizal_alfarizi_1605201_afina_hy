package cs.upi.edu.quiz_1600807_rizal_alfarizi_1605201_afina_hy.Model;

import android.os.Parcel;
import android.os.Parcelable;

public class SensorModel implements Parcelable {
    public int id;
    public String posisi;

    public String getPosisi() {
        return posisi;
    }

    public void setPosisi(String posisi) {
        this.posisi = posisi;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.id);
        dest.writeString(this.posisi);
    }

    public SensorModel() {

    }

    protected SensorModel(Parcel in) {
        this.id = in.readInt();
        this.posisi = in.readString();
    }

    public static final Parcelable.Creator<SensorModel> CREATOR = new Parcelable.Creator<SensorModel>() {
        @Override
        public SensorModel createFromParcel(Parcel source) {
            return new SensorModel(source);
        }

        @Override
        public SensorModel[] newArray(int size) {
            return new SensorModel[size];
        }
    };
}
