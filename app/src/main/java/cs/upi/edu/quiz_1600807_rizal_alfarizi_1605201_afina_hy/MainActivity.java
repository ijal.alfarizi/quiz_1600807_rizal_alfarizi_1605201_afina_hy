//NAMA : RIZAL ALFARIZI
//NIM : 1600807

//NAMA : AFINA HADAINA YUDIANITA
//NIM : 1605201


package cs.upi.edu.quiz_1600807_rizal_alfarizi_1605201_afina_hy;

import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import java.util.ArrayList;

import cs.upi.edu.quiz_1600807_rizal_alfarizi_1605201_afina_hy.Model.SensorModel;
import cs.upi.edu.quiz_1600807_rizal_alfarizi_1605201_afina_hy.servicedb.SensorDB;

public class MainActivity extends AppCompatActivity implements SensorEventListener,View.OnClickListener {
    private SensorManager sm;
    private Sensor senAccel;
    private TextView tvHasil;
    private Button clear,save,load;

    @Override
    protected void onResume() {
        super.onResume();

        sm.registerListener(this, senAccel, SensorManager.SENSOR_DELAY_NORMAL);
    }

    @Override
    protected void onPause() {
        super.onPause();
        sm.unregisterListener(this);
    }

    CustomListAdapter adapter;
    private ArrayList<SensorModel> data;
    private RecyclerView recV;
    private SensorDB dbSensor;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        sensorDetect();
        clear = (Button) findViewById(R.id.clear);
        save = (Button) findViewById(R.id.save);
        load = (Button) findViewById(R.id.load);

        clear.setOnClickListener(this);
        save.setOnClickListener(this);
        load.setOnClickListener(this);


        tvHasil = (TextView) findViewById(R.id.tv_hasil);
        recV = (RecyclerView)findViewById(R.id.rv_posisi);
        recV.setHasFixedSize(true);

        data = new ArrayList<>();

        // membuka database dan menampilkan datanya di reyclerview
        dbSensor = new SensorDB(getApplicationContext());
        dbSensor.open();
        dbSensor.deleteAllSensor();
        dbSensor.close();


        loadData();


    }

    public void sensorDetect(){
        sm = (SensorManager)    getSystemService(getApplicationContext().SENSOR_SERVICE);
        senAccel = sm.getDefaultSensor(Sensor.TYPE_LINEAR_ACCELERATION);
        if (senAccel != null){
            // ada sensor accelerometer!
        }
        else {
            // gagal, tidak ada sensor accelerometer.
            Toast.makeText(this,"Tidak ada sensor", Toast.LENGTH_SHORT).show();

        }

    }

    @Override
    public void onSensorChanged(SensorEvent sensorEvent) {
        double ax=0,ay=0,az=0;
        // menangkap perubahan nilai sensor
        if (sensorEvent.sensor.getType()==Sensor.TYPE_LINEAR_ACCELERATION) {
            ax=sensorEvent.values[0];
            ay=sensorEvent.values[1];
            az=sensorEvent.values[2];
        }

        //CARA MENDAPAT DATA KITA SET BATAS MINIMUM SENSOR SUPAYA TERDETEKSI YAITU -0.5 0.5
        SensorModel d = new SensorModel();
        if(ay>0.5 || ay<-0.5){//ketika hp digerakkan ke depan belakang
            d.posisi = "Atas-bawah";
            data.add(d);
            adapter.notifyDataSetChanged();

//
//            tvHasil.setText("x:"+ax+" y:"+ay+" z:"+az +" Atas-bawah");
        }

        if(ax>0.5 || ax<-0.5){//ketika hp digerakkan ke kiri kanan
            d.posisi = "Kiri-kanan";
            //            tvHasil.setText("x:"+ax+" y:"+ay+" z:"+az+ "Kiri-kanan");
            data.add(d);
            adapter.notifyDataSetChanged();

        }

        if(az>0.5 || az<-0.5){//ketika hp digerakkan keatas kebawah
            d.posisi = "Angkat-turun";
            //            tvHasil.setText("x:"+ax+" y:"+ay+" z:"+az+ "Kiri-kanan");
            data.add(d);
            adapter.notifyDataSetChanged();

        }

    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {


    }

    public void loadData(){

        recV.setLayoutManager(new LinearLayoutManager(this));
        adapter = new CustomListAdapter(this);
        adapter.setListSensor(data);
        recV.setAdapter(adapter);
    }

    @Override
    protected void onDestroy(){
        // menutup database
        dbSensor.close();
        super.onDestroy();
    }


    @Override
    public void onClick(View v) {
        if(v.getId() == R.id.clear){
            data.clear();
            adapter.notifyDataSetChanged();
        }else if(v.getId() == R.id.save){
            dbSensor.open();
            for (SensorModel m :data) {
                dbSensor.insertSensor(m);
            }
            Toast.makeText(this,"Saved", Toast.LENGTH_SHORT).show();

            dbSensor.close();

        }else if(v.getId() == R.id.load){

            dbSensor.open();
            data = dbSensor.getAllSensor();
            dbSensor.close();
            loadData();
            if(data.size()>0){

                Toast.makeText(this,"Loaded", Toast.LENGTH_SHORT).show();
            }else{
                Toast.makeText(this,"Empty", Toast.LENGTH_SHORT).show();

            }
        }
    }
}
